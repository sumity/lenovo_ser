const express = require('express')
const router = express.Router();
var request = require('request');

router.post('/cpt_token', function (req, res) {
    var getDetails = req.body;
    console.log(getDetails);
    if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
        return res.json({"responseCode" : 0,"responseDesc" : "Please select captcha"});
      }
      // Google captcha key
      var secretKey = "6LeO18QUAAAAAFJM3dasptvdAT6zSF4pyARqvPUO";
      // req.connection.remoteAddress will provide IP address of connected user.
      var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'];
      // Hitting GET request to the URL, Google will respond with success or error scenario.
      request(verificationUrl,function(error,response,body) {
        body = JSON.parse(body);
        // Success will be true or false depending upon captcha validation.
        if(body.success !== undefined && !body.success) {
          return res.json({"responseCode" : 0,"responseDesc" : "Failed captcha verification"});
        }
        else{
            getUserDetails(req.body["username"],req.body["password"],(logErr,logData) => {
                if(logErr) return res.json({"responseCode" : 0,"responseDesc" : logErr});
                else return res.json({"responseCode" : 1,"responseDesc" : logData});
            });
        }
      });
});

function getUserDetails(userEm,userPw,callback){
    if(userEm === "admin" && userPw === "admin123"){
        callback(null,"Successful");
    }
    else{
        callback("User not found",null);
    }
}
module.exports = router;