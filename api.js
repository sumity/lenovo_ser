const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors');
const httpPort = 3000;
var https = require('https');
var http = require('http');
const path = require('path');

// import route files
const checkToken = require("./checkToken")
const allowedOrigins = [
    'capacitor://localhost',
    'ionic://localhost',
    'http://localhost',
    'http://localhost:8080',
    'http://localhost:8100',
    'https://localhost',
    'https://localhost:443',
    'http://localhost:3000',
    'https://127.0.0.1',
    'http://127.0.0.1',
    'https://10.122.189.106',
    'http://192.168.100.242',
    'https://18.220.11.47:3000'
];


const app = express();

// Reflect the origin if it's in the allowed list or not defined (cURL, Postman, etc.)
const corsOptions = {
    origin: (origin, callback) => {
        if (allowedOrigins.includes(origin) || !origin) {
            callback(null, true);
        } else {
            callback(new Error(origin + ' Origin not allowed by CORS'));
        }
    }
}

// Enable preflight requests for all routes
app.options('*', cors(corsOptions));

app.use(bodyParser.json())

app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)
app.use(express.static(path.join(__dirname, '')));

app.get('/', (request, response) => {
    response.sendfile('index.html');
})

app.get('/login', (request, response) => {
    response.sendfile('./login/login.html');
})

app.use('/check',checkToken);

var httpServer = http.createServer(app);

httpServer.listen(httpPort, () => console.log(`API running on localhost:${httpPort}`));