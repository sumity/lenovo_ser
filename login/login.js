(function(){
var app = angular.module('testApp', ['vcRecaptcha']);

app.controller('recapCtrl',['vcRecaptchaService','$http','$scope',function(vcRecaptchaService,$http,$scope){
    var vm = this;
    $scope.errorDetails = {
        success : false,
        message : ""
    };
    console.log("this is your app's controller");
    vm.response = null;
    vm.widgetId = null;

    vm.model = {
        key: '6LeO18QUAAAAAAnalqg01Q4QTT5NrSVwyhFbJlb5'
    };

    vm.setResponse = function (response) {
        console.info('Response available');

        vm.response = response;
    };

    vm.setWidgetId = function (widgetId) {
        console.info('Created widget ID: %s', widgetId);

        vm.widgetId = widgetId;
    };

    vm.cbExpiration = function() {
        console.info('Captcha expired. Resetting response object');

        vcRecaptchaService.reload(vm.widgetId);

        vm.response = null;
     };

    vm.submit = function () {
        var valid;
        if(vcRecaptchaService.getResponse() === ""){ //if string is empty
            $scope.errorDetails = {
                    success : true,
                    message : "Please resolve the captcha and submit!"
                };
        }else {
            var post_data = {  
                'username':vm.username,
                'password':vm.password,
                'g-recaptcha-response':vcRecaptchaService.getResponse()  //send g-captcah-reponse to our server
            }
        }

        $http.post('../check/cpt_token',post_data).success(function(response){
            if(response.responseCode === 0){
                console.log("Error :" + response.responseDesc);
                vcRecaptchaService.reload(vm.widgetId);
                $scope.errorDetails = {
                    success : true,
                    message : response.responseDesc
                };
            }else{
                localStorage.setItem("username", vm.username);
                localStorage.setItem("password", vm.password);
                window.location.href = '/';
            }
        })
        .error(function(error){
            console.log("Error :" + error);
            $scope.errorDetails = {
                success : true,
                message : response.responseDesc
            };
            vcRecaptchaService.reload(vm.widgetId);
        })
    };

    // var validateUser = function(){
    // }
}]);

})()